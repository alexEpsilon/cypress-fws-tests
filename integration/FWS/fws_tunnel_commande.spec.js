Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })


describe('TunnelCommandeTest', function() {

    it('tunnel_commande', function() {
        cy.visit('https://feelwellshop.com/')
        cy.get('.product-item').first().click()
        cy.wait(1000)
        cy.get('#product_addtocart_form').within(() => {
          cy.get('input[type="radio"]').first().check({force: true})
          cy.get('button').click()
        })
        
        cy.wait(1000)
        cy.contains('Procéder à la commande').click()
        cy.wait(5000)
        cy.get('#customer-email-fieldset').within(() => {
            cy.get('input[name="username"]').type('idmkr@idmkr.com')
          })
        cy.get('#co-shipping-form').within(() => {
            cy.get('input[name="firstname"]').type('idmkr')
            cy.get('input[name="lastname"]').type('idmkr')
            cy.get('input[name="street[0]"]').type('idmkr')
            cy.get('input[name="city"]').type('idmkr')
            cy.get('input[name="postcode"]').type('1234')
            cy.get('input[name="telephone"]').type('1234567890')
            })
        cy.get('#co-shipping-method-form').within(() => {
            cy.get('input[name="ko_unique_2"]').check()
            cy.get('button').click()
            })
        cy.wait(1000)
        cy.get('#monetico_onetime').click()
        cy.contains('Passer la commande').click()
    })
})
